package chapter02.model;

import java.time.LocalDate;

public class Document {
    private final String name;
    private final DocumentType type;
    private final String location;
    private final LocalDate created;
    private LocalDate modified;

    public Document(final String name,
                    final DocumentType type,
                    final String location) {
        this.name = name;
        this.type = type;
        this.location = location;
        this.created = LocalDate.now();
        this.modified = LocalDate.now();
    }

    public String getName() {
        return name;
    }

    public DocumentType getType() {
        return type;
    }

    public String getLocation() {
        return location;
    }

    public LocalDate getCreated() {
        return created;
    }

    public LocalDate getModified() {
        return modified;
    }

    public void setModified(final LocalDate modified) {
        this.modified = modified;
    }

    @Override
    public String toString() {
        return String.format(
            "%s{name='%s', type=%s, location='%s', created=%s, modified=%s}",
            this.getClass().getName(),
            name,
            type,
            location,
            created,
            modified);
    }
}
