package chapter02.service;

import chapter02.model.Document;
import chapter02.model.DocumentType;

import java.util.List;

import static chapter02.model.DocumentType.DOC;
import static chapter02.model.DocumentType.DOCX;
import static chapter02.model.DocumentType.PDF;
import static chapter02.model.DocumentType.URL;

public class StaticSearchEngine implements SearchEngine {
    private final List<Document> data = List.of(
        new Document(
            "Book Template.pdf", PDF, "/Docs/Template.pdf"
        ),
        new Document(
            "Apress Home Page", URL, "https://apress.com/"
        ),
        new Document(
            "Chapter Template.doc", DOC, "/Docs/Chapter Sample.doc"
        ),
        new Document(
            "Chapter 01.docx", DOCX, "/Docs/Chapter 01.docx"
        )
    );

    @Override
    public List<Document> findByType(final DocumentType documentType) {
        return data.stream()
                   .filter(d -> d.getType() == documentType)
                   .toList();
    }

    @Override
    public List<Document> findAll() {
        return data;
    }
}
