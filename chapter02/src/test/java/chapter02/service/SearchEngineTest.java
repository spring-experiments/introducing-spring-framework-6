package chapter02.service;

import chapter02.model.DocumentType;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SearchEngineTest {
    final SearchEngine engine = new StaticSearchEngine();

    @Test
    void testFindByType() {
        final var documents = engine.findByType(DocumentType.PDF);

        assertThat(documents).hasSize(1);
        assertThat(documents.get(0).getType()).isEqualTo(DocumentType.PDF);
    }

    @Test
    void testFindAll() {
        final var documents = engine.findAll();

        assertThat(documents).hasSize(4);
    }
}
