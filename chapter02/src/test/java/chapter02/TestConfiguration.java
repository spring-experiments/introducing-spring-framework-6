package chapter02;

import chapter02.service.SearchEngine;
import chapter02.service.StaticSearchEngine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfiguration {
    @Bean
    SearchEngine staticSearchEngine() {
        return new StaticSearchEngine();
    }
}
