package chapter02;

import chapter02.model.DocumentType;
import chapter02.service.SearchEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(classes = {TestConfiguration.class})
public class MyDocumentsSpringTest extends AbstractTestNGSpringContextTests {
    @Autowired
    SearchEngine engine;

    @Test
    void testFindByType() {
        final var documents = engine.findByType(DocumentType.PDF);

        assertThat(documents).hasSize(1);
        assertThat(documents.get(0).getType()).isEqualTo(DocumentType.PDF);
    }

    @Test
    void testFindAll() {
        final var documents = engine.findAll();

        assertThat(documents).hasSize(4);
    }
}
