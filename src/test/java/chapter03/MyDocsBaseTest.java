package chapter03;

import chapter03.model.DocumentType;
import chapter03.service.SearchEngine;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public interface MyDocsBaseTest {
    SearchEngine getEngine();

    @Test
    default void testEngineNotNull() {
        assertThat(getEngine()).isNotNull();
    }

    @Test
    default void testFindByType() {
        final var documents = getEngine().findByType(DocumentType.PDF);

        assertThat(documents).hasSize(1);
        assertThat(documents.get(0).getType()).isEqualTo(DocumentType.PDF);
    }

    @Test
    default void testFindAll() {
        final var documents = getEngine().findAll();

        assertThat(documents).hasSize(4);
    }
}
