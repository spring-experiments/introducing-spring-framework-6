package chapter03;

import chapter03.service.SearchEngine;
import chapter03.service.StaticSearchEngine;
import org.testng.annotations.Test;

@Test
public class MyDocsJavaTest implements MyDocsBaseTest {
    SearchEngine engine = new StaticSearchEngine(true);

    @Override
    public SearchEngine getEngine() {
        return engine;
    }
}
