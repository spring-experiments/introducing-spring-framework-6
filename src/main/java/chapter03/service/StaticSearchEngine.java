package chapter03.service;

import chapter03.model.Document;
import chapter03.model.DocumentType;

import java.util.List;

import static chapter03.model.DocumentType.DOC;
import static chapter03.model.DocumentType.DOCX;
import static chapter03.model.DocumentType.PDF;
import static chapter03.model.DocumentType.URL;

public class StaticSearchEngine implements SearchEngine {
    private List<Document> data;

    public StaticSearchEngine() {
        this(false);
    }

    public StaticSearchEngine(final boolean populate) {
        if (populate) {
            populateData();
        }
    }

    public StaticSearchEngine(final List<Document> data) {
        this.data = data;
    }

    public void populateData() {
        populateData(List.of(
            new Document(
                "Book Template.pdf", PDF, "/Docs/Template.pdf"
            ),
            new Document(
                "Apress Home Page", URL, "https://apress.com/"
            ),
            new Document(
                "Chapter Template.doc", DOC, "/Docs/Chapter Sample.doc"
            ),
            new Document(
                "Chapter 01.docx", DOCX, "/Docs/Chapter 01.docx"
            )
        ));
    }

    public void populateData(final List<Document> documents) {
        this.data = documents;
    }

    @Override
    public List<Document> findByType(final DocumentType documentType) {
        return data.stream()
                   .filter(d -> d.getType() == documentType)
                   .toList();
    }

    @Override
    public List<Document> findAll() {
        return data;
    }
}
